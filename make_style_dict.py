#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Simple Recolour - Alpha - some simple image recolouring ( this is the style dict maker ).
# An Inkscape 1.1+ extension
##############################################################################

import inkex


def style_sheet_to_dictionary(self, stylesheet):

    stylesheet_dict = {}

    class_selector_dict = {}
    type_selector_dict = {}
    id_selector_dict = {}

    for rule in stylesheet:
        # Build a dictionary from all rules in style sheet
        selector_name = (str(rule).split("{")[0]).strip()

        if selector_name[0] == '#':
            rule_name = selector_name[1:]
            rule_dict = dict(rule.items())
            id_selector_dict[rule_name] = rule_dict
            continue
        if selector_name[0] == '.':
            rule_name = selector_name[1:]
            rule_dict = dict(rule.items())
            class_selector_dict[rule_name] = rule_dict

        else:
            rule_name = selector_name
            rule_dict = dict(rule.items())
            type_selector_dict[rule_name] = rule_dict

    stylesheet_dict['class_selector_dict'] = class_selector_dict
    stylesheet_dict['type_selector_dict'] = type_selector_dict
    stylesheet_dict['id_selector_dict'] = id_selector_dict

    return stylesheet_dict

def make_pres_attr_dict(self, elements, pres_attr):

    elements_pres_attr_dict = {}

    stylesheet_dict = self.stylesheet_dict

    for element in elements:

        element_pres_attr = ''
        elements_pres_attr_dict[element.get_id()] = {}
        # Check to see if element has Inline Style
        if pres_attr in element.style.keys():

            element_pres_attr = element.style[pres_attr]
            elements_pres_attr_dict[element.get_id()][pres_attr] = element_pres_attr
            element.style[pres_attr] = element_pres_attr
            continue

        # First check if element type has selector
        tag = element.TAG
        if element.TAG in stylesheet_dict['type_selector_dict'].keys():

            if pres_attr in stylesheet_dict['type_selector_dict'][element.TAG]:
                element_pres_attr = stylesheet_dict['type_selector_dict'][element.TAG][pres_attr]

                elements_pres_attr_dict[element.get_id()][pres_attr] = element_pres_attr
                element.style[pres_attr] = element_pres_attr

        # Check for presentation attribute in Class Selector
        # Last class > First class in css precedence
        if element.attrib.get('class'):

            # See if the classes on the element are found in the stylesheet
            for class_name in element.attrib.get('class').split():
                if class_name in stylesheet_dict['class_selector_dict'].keys():
                    if pres_attr in stylesheet_dict['class_selector_dict'][class_name]:
                        element_pres_attr = stylesheet_dict['class_selector_dict'][class_name][pres_attr]

                        elements_pres_attr_dict[element.get_id()][pres_attr] = element_pres_attr
                        element.style[pres_attr] = element_pres_attr

        # Check for presentation attribute in ID Selector
        if element.get_id() in stylesheet_dict['id_selector_dict'].keys():
            if pres_attr in stylesheet_dict['id_selector_dict'][element.get_id()]:
                element_pres_attr = stylesheet_dict['id_selector_dict'][element.get_id()][pres_attr]

                elements_pres_attr_dict[element.get_id()][pres_attr] = element_pres_attr
                element.style[pres_attr] = element_pres_attr

        if element_pres_attr != '':
            continue

        # Check for presentation attribute in attributes (not really called that)

        if element.attrib.get(pres_attr):

            element_pres_attr = element.attrib.get(pres_attr)
            elements_pres_attr_dict[element.get_id()][pres_attr] = element_pres_attr

            element.style[pres_attr] = element_pres_attr

        else:
            elements_pres_attr_dict[element.get_id()][pres_attr] = 'none'
            element.style[pres_attr] = 'none'


    return elements_pres_attr_dict

def make_unique_attr_dict(self, elements, pres_attr):
    None


