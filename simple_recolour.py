#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Simple Recolour - Alpha - some simple image recolouring.
# An Inkscape 1.1+ extension
##############################################################################


import random

from lxml import etree

from copy import deepcopy

import inkex
from inkex import Style, colors, Group

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject
from gi.repository.GdkPixbuf import Pixbuf, InterpType

from make_style_dict import *


# Could not find simplestyle, found this instead in extensions repo
def format_style(my_style):
    """Format an inline style attribute from a dictionary"""
    return ";".join([att + ":" + str(val) for att, val in my_style.items()])


def create_new_group(self, prefix, mode):
    group_id = str(prefix) + '_' + str(random.randrange(100000, 999999))
    new_group = self.svg.add(Group.new('#' + group_id))
    new_group.set('inkscape:groupmode', str(mode))
    new_group.attrib['id'] = group_id
    return new_group


def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None


def make_object_color_dictionary(self):
    None


def gtk3_add_svg_image(svg):
    loader = GdkPixbuf.PixbufLoader()
    loader.write(svg.encode())
    loader.close()
    pixbuf = loader.get_pixbuf()

    image_height = SimpleRecolour.pixbuf_scale_value

    height = pixbuf.props.height
    width = pixbuf.props.width
    # Determines the height of the preview image
    scale_ratio = width / image_height
    scaled_height = int(height / scale_ratio)

    pixbuf = pixbuf.scale_simple(image_height, scaled_height, InterpType.BILINEAR)

    pixbuf_image_frame = builder.get_object("pixbuf_image_frame")

    pixbuf_image_frame.set_from_pixbuf(pixbuf)

    pixbuf_image_frame.show_all()


# Create a semi transparent rectangle path to cover page
def create_page_screen(self):
    parent = self.svg.get_current_layer()

    page_bbox = self.svg.get_page_bbox()

    # inkex.errormsg(page_bbox.width)

    style = {'fill': 'white',
             'opacity': '0.80'
             }

    attribs = {
        'style': format_style(style),
        'd': f'M  {page_bbox.left} {page_bbox.top} {page_bbox.left} {page_bbox.bottom} {page_bbox.right} {page_bbox.bottom} {page_bbox.right} {page_bbox.top} Z'
    }

    page_screen = etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs)

    return page_screen


def change_pres_attr(inkex_self):
    element_list = SimpleRecolour.element_list

    # Get presentation attribute type
    selection_type_radio_group = builder.get_object('fill_rb').get_group()
    for radio in selection_type_radio_group:
        if radio.get_active():
            pres_attr = radio.get_name().split('_pres_attr')[0]

    SimpleRecolour.pres_attr = pres_attr

    SimpleRecolour.adjustable_swatch_list = []

    SimpleRecolour.original_swatch_list = []

    SimpleRecolour.object_color_dictionary = {}

    my_grid = builder.get_object('swatch_grid')

    for swatch in my_grid.get_children():
        my_grid.remove(swatch)

    elements_pres_attr_dict = make_pres_attr_dict(inkex_self, element_list, pres_attr)

    create_swatch_css(inkex_self, elements_pres_attr_dict, pres_attr)

    color_dictionary_to_swatches(inkex_self, elements_pres_attr_dict)

    # my_grid.queue_draw()

class Handler:

    def pixbuf_scale(self, *args):
        SimpleRecolour.pixbuf_scale_value = int(args[0].get_value())

        gtk3_add_svg_image(inkex_self.svg.root.tostring().decode("utf-8"))


    def flip_color_selector(self, *args):

            main_grid = builder.get_object("main_grid")
            color_selection = SimpleRecolour.Widgets.color_selection
            # SimpleRecolour.current_adjustment_eb.disconnect(color_selection.handler_id)
            color_selection_rgba = color_selection.get_current_rgba()

            SimpleRecolour.Widgets.color_selection.destroy()

            color_selection = Gtk.ColorSelection()
            color_selection.set_name('color_selectfgion')
            SimpleRecolour.Widgets.color_selection = color_selection
            color_selection.show()

            SimpleRecolour.Widgets.color_selection = color_selection

            if SimpleRecolour.flip_state == 'vertical':
                main_grid.remove_column(2)
                color_selection.set_current_rgba(color_selection_rgba)
                main_grid.attach(color_selection, 1, 3, 1, 1)
                SimpleRecolour.flip_state = 'horizontal'
                color_selection.handler_id = color_selection.connect('color-changed', Handler.set_swatch_color, SimpleRecolour.current_adjustment_eb)
            else:
                main_grid.remove_row(3)
                color_selection.set_current_rgba(color_selection_rgba)
                main_grid.attach(color_selection, 2, 1, 1, 1)
                SimpleRecolour.flip_state = 'vertical'
                color_selection.handler_id = color_selection.connect('color-changed', Handler.set_swatch_color, SimpleRecolour.current_adjustment_eb)



    def pres_attr_change(self, *args):
        change_pres_attr(inkex_self)

    def null_handler(widget, *args):
        inkex.errormsg('null')

    def reset_swatch_handler(widget, *args):

        pres_attr = SimpleRecolour.pres_attr
        adjustable_swatch_list = SimpleRecolour.adjustable_swatch_list
        original_swatch_list = SimpleRecolour.original_swatch_list

        widget.stop_emission_by_name('button-press-event')

        index = widget.index

        adjustable_swatch_eb = adjustable_swatch_list[index]

        adjustable_swatch_eb_style_context = adjustable_swatch_eb.get_style_context()

        current_swatch_color_class = adjustable_swatch_eb.background_color_class_name

        adjustable_swatch_eb_style_context.remove_class(current_swatch_color_class)
        adjustable_swatch_eb_style_context.add_class(original_swatch_list[index].color_class)
        adjustable_swatch_eb.RGBA = original_swatch_list[index].RGBA
        adjustable_swatch_eb.set_tooltip_markup(f'rgb{original_swatch_list[index].string_rgb_color}')

        for my_object in adjustable_swatch_eb.attached_objects:
            my_object.style[pres_attr] = original_swatch_list[index].background_color

        gtk3_add_svg_image(inkex_self.svg.root.tostring().decode("utf-8"))

        return None

    def adjustable_swatch_change(widget, *args):

        SimpleRecolour.is_swatch_selected = True

        # Remove Highlight Class from all Swatches
        swatch_grid = builder.get_object('swatch_grid')
        swatch_grid_children = swatch_grid.get_children()
        for child in swatch_grid_children:
            child_style_context = child.get_style_context()
            child_style_context.remove_class('swatchBorderRed')

        # Highlight current Swatch
        adjustable_swatch = widget
        adjustable_swatch_style_context = adjustable_swatch.get_style_context()
        adjustable_swatch_style_context.add_class("swatchBorderRed")

        # color_selection = builder.get_object('color_selection')
        color_selection = SimpleRecolour.Widgets.color_selection
        # color_selection.set_size_request(300, 100)

        if hasattr(color_selection, 'handler_id'):
            color_selection.disconnect(color_selection.handler_id)

        current_swatch_color = widget.RGBA

        color_selection.set_current_rgba(current_swatch_color)
        SimpleRecolour.current_adjustment_eb = widget

        current_handler_id = color_selection.connect('color-changed', Handler.set_swatch_color, widget)

        color_selection.handler_id = current_handler_id

    def adjustment_swatch_mouse_enter(widget, *args):

        # Creat a simple semi transparent rectangle path to cover the rest
        # Of the elements.

        highlight_layer = create_new_group(inkex_self, 'highlight_layer', 'layer')

        page_screen = create_page_screen(inkex_self)
        inkex_self.highlight_layer = highlight_layer
        highlight_layer.append(page_screen)

        widget.attached_highlighted_objects = []

        for attached_object in widget.attached_objects:
            attached_object_transform = attached_object.composed_transform()
            highlighted_object = attached_object.duplicate()
            highlighted_object.transform = attached_object_transform
            highlighted_object.style['stroke'] = 'black'
            highlighted_object.style['stroke-width'] = '5'
            highlighted_object.style['stroke-dasharray'] = '2,2'
            highlighted_object.set_id('highlighted_object')
            highlight_layer.append(highlighted_object)

        gtk3_add_svg_image(inkex_self.svg.root.tostring().decode("utf-8"))

    def adjustment_swatch_mouse_exit(widget, *args):

        for highlighted_object in widget.attached_highlighted_objects:
            highlighted_object.delete()

        inkex_self.highlight_layer.delete()

        gtk3_add_svg_image(inkex_self.svg.root.tostring().decode("utf-8"))

    def set_swatch_color(widget, *args):

        pres_attr = SimpleRecolour.pres_attr

        is_swatch_selected = SimpleRecolour.is_swatch_selected
        if is_swatch_selected == False:
            debug_label = builder.get_object('debug_label')
            debug_label.set_text('** Please Seleect A Swatch **')
            return

        current_rgba = widget.get_current_rgba()

        debug_label = builder.get_object('debug_label')
        debug_label.set_text(str(current_rgba))
        # adjustable_swatch_eb = args[1]
        adjustable_swatch_eb = args[0]
        rgb_string = make_rgb_string(current_rgba)

        adjustable_swatch_eb.string_rgb_color = rgb_string
        adjustable_swatch_eb.set_tooltip_markup(rgb_string)

        adjustable_swatch_eb_style_context = adjustable_swatch_eb.get_style_context()
        class_name = adjustable_swatch_eb.get_name() + 'new_background-color'

        adjustable_swatch_eb.background_color_class_name = class_name

        swatch_css_provider = Gtk.CssProvider()
        # swatch_css = '.yellow {background-color:yellow;}'
        swatch_css = f'.{class_name} {{background-color:{rgb_string}}}\n '

        swatch_css_provider.load_from_data(bytes(swatch_css.encode()))
        current_swatch_color = adjustable_swatch_eb.background_color_class_name

        adjustable_swatch_eb_style_context.remove_class(current_swatch_color)
        adjustable_swatch_eb_style_context.add_class(class_name)
        Gtk.StyleContext.add_provider_for_screen(screen, swatch_css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        for my_object in adjustable_swatch_eb.attached_objects:
            my_object.style[pres_attr] = rgb_string

        args[0].RGBA = current_rgba

        gtk3_add_svg_image(inkex_self.svg.root.tostring().decode("utf-8"))


    def print_color(widget, *args):
        # widget.emit('button-press-event', 'hello')

        current_rgba = widget.get_rgba()
        # Arg 1 should be the adjustable colour swatch
        adjustable_swatch = args[1]
        adjustable_swatch.set_rgba(current_rgba)


def make_rgb_string(current_rgba):
    rgba_r = int(current_rgba.red * 255)
    rgba_g = int(current_rgba.green * 255)
    rgba_b = int(current_rgba.blue * 255)
    rgba_string = str(f'rgb({rgba_r}, {rgba_g}, {rgba_b})')
    return rgba_string

def rgb_list_to_RGBA(my_rgb_color):

    RGBA = Gdk.RGBA()
    RGBA.red = my_rgb_color[0]
    RGBA.green = my_rgb_color[1]
    RGBA.blue = my_rgb_color[2]

    return RGBA


def add_grid_row(self, grid_id, my_object, index):
    adjustable_swatch_list = SimpleRecolour.adjustable_swatch_list
    pres_attr = SimpleRecolour.pres_attr

    my_rgb_color = get_object_color(my_object)

    # Set 'url' fills etc ( gradient / pattern etc ) as invalid

    if 'none' in my_object.style[pres_attr] or 'url' in my_object.style[pres_attr]:
        # inkex.errormsg('failed')
        return index - 1
    if my_rgb_color == 'invalid':
        return index - 1

    string_rgb_color = (str([int(i * 255) for i in my_rgb_color]))
    string_rgb_color = string_rgb_color.replace('[', '(')
    string_rgb_color = string_rgb_color.replace(']', ')')

    # Loop through existing swatches
    for existing_swatch in adjustable_swatch_list:
        if string_rgb_color == existing_swatch.string_rgb_color:
            existing_swatch.attached_objects.append(my_object)
            return index - 1

    my_grid = builder.get_object(grid_id)

    original_swatch_eb = Gtk.EventBox()
    original_swatch_eb.set_tooltip_markup(f'Click to reset to rgb{string_rgb_color}')
    original_swatch_eb.index = index
    original_swatch_eb_style_context = original_swatch_eb.get_style_context()
    original_swatch_eb.color_class = my_object.get_id()
    original_swatch_eb_style_context.add_class(f'{my_object.get_id()}')

    original_swatch_eb.string_rgb_color = string_rgb_color
    # inkex.errormsg(f'fault {swatch_css_dict}')
    original_swatch_eb.background_color = swatch_css_dict[my_object.get_id()]
    original_swatch_eb.set_size_request(20, 20)
    original_swatch_eb.RGBA = rgb_list_to_RGBA(my_rgb_color)
    original_swatch_eb.show()
    my_grid.attach(original_swatch_eb, 0, index, 1, 1)
    original_swatch_eb.connect('button-press-event', Handler.reset_swatch_handler)
    original_swatch_list = SimpleRecolour.original_swatch_list
    original_swatch_list.append(original_swatch_eb)

    adjustable_swatch_eb = Gtk.EventBox()

    # Add colour string to swatch - for comparison later
    adjustable_swatch_eb.string_rgb_color = string_rgb_color

    if type(my_rgb_color) != None:
        adjustable_swatch_eb.attached_objects = []
        adjustable_swatch_eb.attached_objects.append(my_object)
        adjustable_swatch_eb.connect('button-press-event', Handler.adjustable_swatch_change)
        adjustable_swatch_eb.connect('enter-notify-event', Handler.adjustment_swatch_mouse_enter)
        adjustable_swatch_eb.connect('leave-notify-event', Handler.adjustment_swatch_mouse_exit)
        adjustable_swatch_eb.set_name(f'adjustable_swatch_eb_{index}')

        adjustable_swatch_eb_name = my_object.get_id()

        adjustable_swatch_eb.set_name(adjustable_swatch_eb_name)

        adjustable_swatch_eb_style_context = adjustable_swatch_eb.get_style_context()
        adjustable_swatch_eb_style_context.add_class(f'{my_object.get_id()}')
        adjustable_swatch_eb.background_color_class_name = my_object.get_id()

        adjustable_swatch_eb.set_size_request(20, 20)

        adjustable_swatch_eb.index = index
        adjustable_swatch_list.append(adjustable_swatch_eb)
        adjustable_swatch_eb.set_visible(True)
        adjustable_swatch_eb.show()

        adjustable_swatch_eb.RGBA = rgb_list_to_RGBA(my_rgb_color)
        adjustable_swatch_eb.set_tooltip_markup(f'rgb{string_rgb_color}')

        # my_grid.attach(adjustable_color_swatch, 1, index, 1, 1)

        my_grid.attach(adjustable_swatch_eb, 1, index, 1, 1)

        return index


def create_swatch_css(self, elements_pres_attr_dict, press_attr):
    global swatch_css
    swatch_css = ''
    global swatch_css_dict
    swatch_css_dict = {}
    for item in elements_pres_attr_dict.items():

        current_press_attr = item[1][press_attr]
        if current_press_attr != 'none' and 'url' not in current_press_attr:
            swatch_css = swatch_css + f'.{item[0]} {{background-color:{current_press_attr}}}\n '
            swatch_css_dict[item[0]] = current_press_attr
    # inkex.errormsg(swatch_css)
    screen = Gdk.Screen.get_default()

    dummy_css = '.yellow {background-color:yellow;}'

    object_css_provider = Gtk.CssProvider()
    object_css_provider.load_from_data(bytes(swatch_css.encode()))
    # object_css_provider.load_from_data(bytes(dummy_css.encode()))

    # inkex.errormsg(swatch_css_dict)
    Gtk.StyleContext.add_provider_for_screen(screen, object_css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


def get_object_color(my_object):
    pres_attr = SimpleRecolour.pres_attr

    my_object_style = my_object.style

    my_object_pres_attr = my_object_style[pres_attr]

    # Check for no fill or gradient fill
    if my_object_pres_attr.lower() != 'none' and 'url' not in my_object_pres_attr:

        pres_attr_rgb = list(colors.Color.parse_str(my_object_pres_attr)[1])
        pres_attr_rgb_float = [int(channel) / 255 for channel in pres_attr_rgb]

    else:
        pres_attr_rgb_float = 'invalid'

    return pres_attr_rgb_float


def get_object_alpha(self, my_object):
    my_object_style = my_object.style
    my_object_alpha = my_object_style['opacity']
    return float(my_object_alpha)


def color_dictionary_to_swatches(self, elements_pres_attr_dict):
    index = 0
    for element in elements_pres_attr_dict:
        my_object = self.svg.getElementById(element)
        index = add_grid_row(self, 'swatch_grid', my_object, index)
        index += 1


def run_gtk(self, elements_pres_attr_dict, pres_attr):
    # Load stylesheet
    global screen
    screen = Gdk.Screen.get_default()
    global provider
    provider = Gtk.CssProvider()
    provider.load_from_path("simple_recolour.css")
    Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    global builder
    builder = Gtk.Builder()
    builder.add_from_file('simple_recolour.glade')

    global window
    window = builder.get_object("main_window")
    builder.connect_signals(Handler())
    window.connect("destroy", Gtk.main_quit)
    window.show_all()

    # Dummy Class to allow widget recall
    class Widgets:
        None

    SimpleRecolour.Widgets = Widgets
    SimpleRecolour.Widgets.color_selection = builder.get_object('color_selection')
    SimpleRecolour.flip_state = 'vertical'

    SimpleRecolour.pixbuf_scale_value = 500



    debug_label = builder.get_object("debug_label")
    debug_label.hide()

    color_dictionary_to_swatches(self, elements_pres_attr_dict)

    if len(SimpleRecolour.adjustable_swatch_list) > 1:

        SimpleRecolour.current_adjustment_eb = SimpleRecolour.adjustable_swatch_list[0]

    gtk3_add_svg_image(self.svg.root.tostring().decode("utf-8"))

    Gtk.main()

class SimpleRecolour(inkex.EffectExtension):

    def effect(self):
        global inkex_self
        inkex_self = self

        elements = self.svg.selected

        SimpleRecolour.element_list = []
        element_list = SimpleRecolour.element_list

        SimpleRecolour.adjustable_swatch_list = []

        SimpleRecolour.original_swatch_list = []
        # original_swatch_list = SimpleRecolour.original_swatch_list

        # global pres_attr - the attribute we wish to change
        SimpleRecolour.pres_attr = 'fill'
        pres_attr = SimpleRecolour.pres_attr

        SimpleRecolour.object_color_dictionary = {}

        SimpleRecolour.is_swatch_selected = False

        if len(elements) > 0:

            for element in elements:
                if element.TAG != 'g':
                    element_list.append(element)

        if len(elements) < 1 or len(element_list) < 1:
            element_list = self.svg.xpath(
                '//svg:circle | //svg:ellipse | //svg:line | //svg:path | //svg:text | //svg:polygon | //svg:polyline | //svg:rect')
            if len(element_list) < 1:
                inkex.errormsg('This document appears to be empty ?')
                return

        self.stylesheet_dict = style_sheet_to_dictionary(self, self.svg.stylesheet)

        elements_pres_attr_dict = make_pres_attr_dict(self, element_list, pres_attr)

        # inkex.errormsg(elements_pres_attr_dict)
        # return

        create_swatch_css(self, elements_pres_attr_dict, pres_attr)

        SimpleRecolour.element_list = element_list

        run_gtk(self, elements_pres_attr_dict, pres_attr)


if __name__ == '__main__':
    SimpleRecolour().run()
